# Frontend

Frontend-Projekt von Gruppe 2 im Modul IvA im Sommersemester 2022
Dieses Projekt beinhaltet Angular.js Code und Syntax

## Dependencies

Benötigt wird Node.js, insbesondere npm. Von Vorteil ist Angular CLI

## Getting startet

Projekt clonen. Anschließend Befehl `npm install` ausführen um alle Dependencies zu installieren.

## Development server

Befehl `ng serve` um Projekt zu starten. Falls die Angular CLI nicht installiert ist, mit `npm start` ausführen.  Navigieren zu `http://localhost:4200/`.
