import { createAction, props } from '@ngrx/store';

export const dropKeys = createAction('[Crypto] keys dropped');

export const setKey = createAction('[Crypto] getKey', props<{ key: string }>());

export const setIV = createAction('[Crypto] getIV', props<{ iv: string }>());
