import { createAction, props } from '@ngrx/store';

export const adminLogin = createAction(
  '[Login] admin login',
  props<{ success: boolean }>()
);

export const supervisorLogin = createAction(
  '[Login] supervisor login',
  props<{ success: boolean }>()
);

export const logout = createAction('[Login] logout');
