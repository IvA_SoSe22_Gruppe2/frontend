import { State } from '../reducers';
import { createSelector } from '@ngrx/store';
import { LoginState } from '../reducers/login.reducer';

export const select = (state: State) => state.login;

export const adminLogin = createSelector<State, LoginState, any>(
  select,
  (state: LoginState) => state.adminVerified
);

export const supervisorLogin = createSelector<State, LoginState, boolean>(
  select,
  (state: LoginState) => state.supervisorVerified
);
