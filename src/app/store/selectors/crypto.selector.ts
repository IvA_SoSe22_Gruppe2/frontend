import { State } from '../reducers';
import { createSelector } from '@ngrx/store';
import { CryptoState } from '../reducers/crypto.reducer';

export const select = (state: State) => state.crypto;

export const getKey = createSelector<State, CryptoState, string>(
  select,
  (state: CryptoState) => state.key
);

export const getIV = createSelector<State, CryptoState, string>(
  select,
  (state: CryptoState) => state.iv
);
