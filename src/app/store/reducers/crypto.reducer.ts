import { createReducer, on, Action } from '@ngrx/store';
import { dropKeys, setIV, setKey } from '../actions/crypto.actions';

export interface CryptoState {
  key: string;
  iv: string;
}

export const initialState: CryptoState = {
  key: "qTr{@^h`F&_`90/ja9!'dcmh3!uw?&=?",
  iv: '4&\\~V(,A.l-&Pf65',
};

const cryptoReducer = createReducer(
  initialState,
  on(dropKeys, () => ({
    key: '',
    iv: '',
  })),
  on(setKey, (state, { key }) => ({
    ...state,
    key: key,
  })),
  on(setIV, (state, { iv }) => ({
    ...state,
    iv: iv,
  }))
);

export function reducer(
  state: CryptoState | undefined,
  action: Action
): CryptoState {
  return cryptoReducer(state as CryptoState, action as Action);
}
