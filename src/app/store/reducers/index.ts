import { LoginState } from './login.reducer';
import { ActionReducerMap } from '@ngrx/store';
import * as fromLogin from './login.reducer';
import * as fromCrypto from './crypto.reducer';
import { CryptoState } from './crypto.reducer';

export interface State {
  login: LoginState;
  crypto: CryptoState;
}

export const reducers: ActionReducerMap<State> = {
  login: fromLogin.reducer,
  crypto: fromCrypto.reducer,
};
