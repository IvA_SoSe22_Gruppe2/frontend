import { createReducer, on, Action } from '@ngrx/store';
import { adminLogin, supervisorLogin, logout } from '../actions/login.actions';

export interface LoginState {
  adminVerified: boolean;
  supervisorVerified: boolean;
}

export const initialState: LoginState = {
  adminVerified: false,
  supervisorVerified: false,
};

const loginReducer = createReducer(
  initialState,
  on(logout, (state) => ({
    adminVerified: false,
    supervisorVerified: false,
  })),
  on(adminLogin, (state, { success }) => ({
    ...state,
    adminVerified: success,
    supervisorVerified: false,
  })),
  on(supervisorLogin, (state, { success }) => ({
    ...state,
    adminVerified: false,
    supervisorVerified: success,
  }))
);

export function reducer(
  state: LoginState | undefined,
  action: Action
): LoginState {
  return loginReducer(state as LoginState, action as Action);
}
