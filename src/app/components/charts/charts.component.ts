import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ActivityCollection } from '../../interfaces/activityCollection';
import { Activity } from '../../interfaces/activity';
import { ActivityEvent } from '../../interfaces/event';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss'],
})
export class ChartsComponent implements OnInit {
  @Input() chartType: string = 'bar';

  productSales: any[] | undefined;
  productSalesMulti: any[] | undefined;

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Sportart';
  showYAxisLabel = true;
  yAxisLabel = '';
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'],
  };

  cardColor: string = '#232837';

  events$: Observable<any> | undefined;
  activities$: Observable<any> | undefined;

  events: Array<ActivityEvent> | undefined;
  activities: Array<Activity> | undefined;

  allActivityData: Array<ActivityCollection> = [];

  usableData: Array<any> = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.events$ = this.http.get<any>(
      environment.apiURL + '/Event/getAllEvents',
      {
        observe: 'response',
      }
    );

    this.activities$ = this.http.get<any>(
      environment.apiURL + '/Activity/getAllActivities',
      {
        observe: 'response',
      }
    );

    this.calculateEventData();
  }

  calculateEventData() {
    this.activities$?.subscribe((act) => {
      this.activities = act.body;
      if (this.activities) {
        this.activities.forEach((one) => {
          this.allActivityData?.push({
            activityId: one.activityId,
            avgSpeed: 0,
            calories: 0,
            activityName: one.activityName,
            distanceInKm: 0,
            duration: 0,
            entries: 0,
          });
        });

        this.events$?.subscribe((evt) => {
          this.events = evt.body;

          this.events?.forEach((one) => {
            let cond = (element: any) => element.activityId === one.activityId;
            let index = this.allActivityData?.findIndex(cond);
            if (this.allActivityData && typeof index !== 'undefined') {
              let result = this.allActivityData[index];
              result.entries = result.entries + 1;
              result.avgSpeed = result.avgSpeed + one.avgSpeed;
              result.calories = result.calories + one.calories;
              result.distanceInKm = result.distanceInKm + one.distanceInKm;
              if (one.duration !== 0) {
                result.duration =
                  result.duration + Math.round(one.duration / (60 * 10000000));
              }
            }
          });
          this.allActivityData.forEach((activity) => {
            if (activity.avgSpeed !== 0) {
              activity.avgSpeed = activity.avgSpeed / activity.entries;
            }
          });
        });
      }
    });
  }

  getUsableData(key: string): Array<any> {
    this.usableData = [];
    this.yAxisLabel = key;
    if (this.allActivityData?.length) {
      this.allActivityData.forEach((one) => {
        this.usableData.push({
          name: one.activityName,
          value:
            key === 'Durchschnittliche Geschwindigkeit'
              ? one.avgSpeed
              : key === 'Kalorien'
              ? one.calories
              : key === 'Distanz in KM'
              ? one.distanceInKm
              : key === 'Dauer'
              ? one.duration
              : key === 'Anzahl der Einträge'
              ? one.entries
              : 0,
        });
      });
      return this.usableData;
    } else return [];
  }
}
