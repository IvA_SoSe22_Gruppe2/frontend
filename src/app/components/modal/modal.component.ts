import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() title: string = '';
  @Input() text: string = '';
  @Input() button: string = '';
  @Output() closeEvent = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  close() {
    this.closeEvent.emit(false);
  }
}
