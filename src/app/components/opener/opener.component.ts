import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-opener',
  templateUrl: './opener.component.html',
  styleUrls: ['./opener.component.scss'],
})
export class OpenerComponent implements OnInit {
  @Input() headline: string = '';
  @Input() text: string = '';
  @Input() buttonText: string = '';
  @Input() buttonAction: { url: boolean; action: string } = {
    url: false,
    action: '',
  };
  @Input() headImage: string = '';
  @Input() fullPage: boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
