import { Component, Inject, OnInit } from '@angular/core';
import { Route, Router, NavigationEnd } from '@angular/router';
import { State } from '../../store/reducers';
import { Store } from '@ngrx/store';
import { LoginActions } from '../../store/actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  route: string = '';
  logout: boolean = false;

  constructor(private router: Router, private readonly store: Store<State>) {}

  ngOnInit(): void {
    this.router.events.subscribe((routeChange) => {
      if (routeChange instanceof NavigationEnd) {
        this.route = routeChange.url;
        if (this.route.includes('supervisor') || this.route.includes('admin')) {
          this.logout = true;
          console.log(this.logout);
        } else {
          this.logout = false;
        }
      }
    });
  }

  logging() {
    this.store.dispatch(LoginActions.logout());
  }
}
