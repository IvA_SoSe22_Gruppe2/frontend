import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import SwiperCore, { Keyboard, Pagination, Navigation, Virtual } from 'swiper';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpResponse } from '@angular/common/http';

SwiperCore.use([Keyboard, Pagination, Navigation, Virtual]);

@Component({
  selector: 'app-news-swiper',
  templateUrl: './newsSwiper.component.html',
  styleUrls: ['./newsSwiper.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NewsSwiperComponent implements OnInit, OnChanges {
  @Input() edit: boolean = false;
  @Input() reload: boolean = true;

  @Output() editEvent = new EventEmitter<any>();
  @Output() deleteEvent = new EventEmitter<any>();

  news$: Observable<HttpResponse<any>> | undefined;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.news$ = this.http.get<any>(environment.apiURL + '/News/getAllNews', {
      observe: 'response',
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.news$ = this.http.get<any>(environment.apiURL + '/News/getAllNews', {
      observe: 'response',
    });
  }

  shortenHeader(headline: string): string {
    return headline.substring(0, 99);
  }

  editNews(news: any) {
    this.editEvent.emit(news);
  }

  deleteNews(news: any) {
    this.deleteEvent.emit(news);
  }
}
