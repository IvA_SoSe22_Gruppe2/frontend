import { Component, OnInit } from '@angular/core';
import { LoginSelectors } from '../../store/selectors';
import { State } from '../../store/reducers';
import { Store } from '@ngrx/store';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-second-level-header',
  templateUrl: './second-level-header.component.html',
  styleUrls: ['./second-level-header.component.scss'],
})
export class SecondLevelHeaderComponent implements OnInit {
  admin: boolean = false;
  supervisor: boolean = false;

  constructor(
    private readonly store: Store<State>,
    public route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.store.select(LoginSelectors.adminLogin).subscribe((accepted) => {
      if (accepted) {
        this.admin = true;
      } else {
        this.admin = false;
      }
    });
    this.store.select(LoginSelectors.supervisorLogin).subscribe((accepted) => {
      if (accepted) {
        this.supervisor = true;
      } else {
        this.supervisor = false;
      }
    });
  }

  navigate(slug: string): void {
    if (slug === 'back') {
      if (this.admin) {
        this.router.navigate(['/admin']);
      }
      if (this.supervisor) {
        this.router.navigate(['supervisor']);
      }
    } else {
      if (this.admin) {
        this.router.navigate(['admin', slug]);
      }
      if (this.supervisor) {
        this.router.navigate(['supervisor', slug]);
      }
    }
  }
}
