export interface ActivityEvent {
  activityId: number;
  avgSpeed: number;
  calories: number;
  distanceInKm: number;
  duration: number;
  endDate: string;
  eventId: number;
  startDate: string;
  userId: number;
}

export interface ActivityEventDownload {
  activityName: string;
  avgSpeed: number;
  calories: number;
  distanceInKm: number;
  duration: number;
  endDate: string;
  eventId: number;
  startDate: string;
  userId: number;
}
