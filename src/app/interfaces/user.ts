export interface User {
  lastName: string;
  firstName: string;
  gender: 'male' | 'female' | 'diverse';
  height: number;
  weight: number;
  age: string; //date format -> dd/mm/yy:Tss:mm:hh
  profilePicture: string; //base64
  email: string;
  password: string; //to be hashed as sha256
  role: 'user' | 'admin' | 'supervisor';
}
