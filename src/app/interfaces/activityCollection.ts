export interface ActivityCollection {
  activityId: number;
  activityName: string;
  avgSpeed: number;
  calories: number;
  distanceInKm: number;
  duration: number;
  entries: number;
}
