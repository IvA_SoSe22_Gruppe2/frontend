export interface News {
  id: number;
  newsContent: string;
  newsDate: string;
  newsPicture: string;
  newsHeader: string;
}
