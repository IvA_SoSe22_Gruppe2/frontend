export interface Activity {
  activityId: number;
  activityName: string;
  category: any;
}
