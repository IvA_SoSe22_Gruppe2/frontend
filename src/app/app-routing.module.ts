import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './views/about/about.component';
import { ContactComponent } from './views/contact/contact.component';
import { DataProtectionComponent } from './views/data-protection/data-protection.component';
import { HomeComponent } from './views/home/home.component';
import { ImprintComponent } from './views/imprint/imprint.component';
import { LoginComponent } from './views/login/login.component';
import { NewsComponent } from './views/news/news.component';
import { RegisterComponent } from './views/register/register.component';
import { AdminComponent } from './views/admin/admin.component';
import { SupervisorComponent } from './views/supervisor/supervisor.component';
import { VerifyComponent } from './views/verify/verify.component';
import { PiechartComponent } from './views/secured-views/piechart/piechart.component';
import { BarchartComponent } from './views/secured-views/barchart/barchart.component';
import { TableComponent } from './views/secured-views/table/table.component';
import { DownloadComponent } from './views/secured-views/download/download.component';
import { UserEditComponent } from './views/secured-views/user-edit/user-edit.component';
import { AdminEditComponent } from './views/secured-views/admin-edit/admin-edit.component';
import { NewsEditComponent } from './views/secured-views/news-edit/news-edit.component';
import { NewsManagementComponent } from './views/secured-views/news-management/news-management.component';

//ToDo add verification of all routes -> Service?
const routes: Routes = [
  { path: 'news/:id', component: NewsComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'data-protection', component: DataProtectionComponent },
  { path: 'imprint', component: ImprintComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'supervisor', component: SupervisorComponent },
  { path: 'verify/:hash', component: VerifyComponent },
  { path: 'admin/piechart', component: PiechartComponent },
  { path: 'admin/barchart', component: BarchartComponent },
  { path: 'admin/table', component: TableComponent },
  { path: 'admin/download', component: DownloadComponent },
  { path: 'admin/user-edit', component: UserEditComponent },
  { path: 'admin/admin-edit', component: AdminEditComponent },
  { path: 'admin/news-edit', component: NewsEditComponent },
  { path: 'admin/news-management', component: NewsManagementComponent },
  { path: 'supervisor/piechart', component: PiechartComponent },
  { path: 'supervisor/barchart', component: BarchartComponent },
  { path: 'supervisor/table', component: TableComponent },
  { path: 'supervisor/download', component: DownloadComponent },
  { path: 'supervisor/user-edit', component: UserEditComponent },
  { path: 'supervisor/news-edit', component: NewsEditComponent },
  { path: 'supervisor/news-management', component: NewsManagementComponent },

  { path: '', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
