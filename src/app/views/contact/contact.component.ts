import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  contactGroup!: FormGroup;

  modal: boolean = false;
  modalTitle: string = 'Danke für deine Nachricht!';
  modalText: string =
    'Wir werden uns so bald wie möglich mit dir in Verbindung setzen.';

  constructor() {}

  ngOnInit(): void {
    this.contactGroup = new FormGroup({
      firstname: new FormControl('', { validators: [Validators.required] }),
      name: new FormControl('', { validators: [Validators.required] }),
      mail: new FormControl('', { validators: [Validators.required] }),
      text: new FormControl('', { validators: [Validators.required] }),
    });
  }

  closeModal(value: boolean) {
    this.modal = false;
  }

  showPopup(): void {
    if (this.contactGroup.valid) {
      this.modal = true;
      this.modalTitle = 'Danke für deine Nachricht!';
      this.modalText =
        'Wir werden uns so bald wie möglich mit dir in Verbindung setzen.';
    } else {
      this.modal = true;
      this.modalTitle = 'Fehler';
      this.modalText =
        'Bitte fülle vor absenden alle erforderlichen Felder aus!';
    }
  }
}
