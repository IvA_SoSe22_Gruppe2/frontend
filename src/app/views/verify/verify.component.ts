import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import { CryptoService } from '../../service/crypto.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss'],
})
export class VerifyComponent implements OnInit {
  hash: string = '-1';
  email: string = 'error';
  result: 'loading' | 'success' | 'error' = 'success';

  constructor(
    private router: Router,
    private http: HttpClient,
    private readonly store: Store<State>,
    private crypto: CryptoService
  ) {
    this.router.events.subscribe((routeChange) => {
      if (routeChange instanceof NavigationEnd) {
        this.hash = routeChange.url.substring(
          routeChange.url.lastIndexOf('/') + 1
        );
        if (this.hash !== '-1' && this.hash !== '') {
          this.email = this.crypto.decrypt(this.hash);
          if (this.email !== 'error') {
            this.http
              .post<any>(
                environment.apiURL + '/User/activate',
                {},
                {
                  observe: 'response',
                  params: {
                    email: this.email,
                  },
                }
              )
              .subscribe(
                (msg) => {
                  if (msg.status === 200) {
                    this.result = 'success';
                  } else {
                    this.result = 'error';
                  }
                },
                (error: any) => {
                  this.result = 'error';
                }
              );
          }
        }
      }
    });
  }

  ngOnInit(): void {}
}
