import { Component, OnInit, SecurityContext } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { News } from '../../interfaces/news';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit {
  id: number = -1;
  news: News | undefined;
  imagePath: SafeResourceUrl | undefined;

  constructor(private router: Router, private http: HttpClient) {
    this.router.events.subscribe((routeChange) => {
      if (routeChange instanceof NavigationEnd) {
        this.id = parseInt(
          routeChange.url.substring(routeChange.url.lastIndexOf('/') + 1)
        );
        if (this.id !== -1 && this.id) {
          this.http
            .get<any>(environment.apiURL + '/News/' + this.id, {
              observe: 'response',
            })
            .subscribe((msg) => {
              this.news = msg.body[0];
              if (this.news?.newsPicture !== '' && this.news?.newsPicture) {
              }
            });
        }
      }
    });
  }

  ngOnInit(): void {}
}
