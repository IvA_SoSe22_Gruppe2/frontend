import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { LoginActions } from '../../store/actions';
import { Router } from '@angular/router';
import { State } from '../../store/reducers';
import { CryptoService } from '../../service/crypto.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginGroup!: FormGroup;

  modal: boolean = false;
  modalTitle: string = '';
  modalText: string = '';

  constructor(
    private http: HttpClient,
    private readonly store: Store<State>,
    private router: Router,
    private crypto: CryptoService
  ) {}

  ngOnInit(): void {
    this.loginGroup = new FormGroup({
      mail: new FormControl('', { validators: [Validators.required] }),
      password: new FormControl('', {
        validators: [Validators.required, Validators.minLength(6)],
      }),
    });
  }

  closeModal(value: boolean) {
    this.modal = false;
  }

  markAsDirty(path: string) {
    this.loginGroup?.get(path)?.markAsDirty();
  }

  login() {
    if (this.loginGroup.valid) {
      this.http
        .get<any>(environment.apiURL + '/User/loginValidation', {
          observe: 'response',
          params: {
            email: this.loginGroup.get('mail')?.value,
            password: this.crypto.encrypt(
              this.loginGroup.get('password')?.value
            ),
          },
        })
        .subscribe(
          (msg) => {
            if (msg.status === 200) {
              if (msg.body[0].role === 'admin') {
                this.store.dispatch(LoginActions.adminLogin({ success: true }));
                this.router.navigate(['/admin']);
              } else if (msg.body[0].role === 'supervisor') {
                this.store.dispatch(
                  LoginActions.supervisorLogin({ success: true })
                );
                this.router.navigate(['/supervisor']);
              } else {
                this.modalTitle = 'Unberechtigter Zugriff';
                this.modalText =
                  'Sorry, leider hast du keinen Zugriff hier. <br> Logge dich in deiner App ein!';
                this.modal = true;
              }
            }
          },
          (error: any) => {
            if (error.status === 404) {
              this.modalTitle = 'Fehler';
              this.modalText =
                'Der Nutzername oder das Passwort sind falsch. <br> Bitte versuchen Sie es erneut.';
              this.modal = true;
            } else {
              this.modalTitle = 'Fehler';
              this.modalText =
                'Ein unbekannter Fehler ist aufgetreten.<br> Bitte versuchen Sie es später erneut!';
              this.modal = true;
            }
          }
        );
    } else {
      this.modalTitle = 'Fehler';
      this.modalText =
        'Nicht alle Felder wurden richtig ausgefüllt. <br> Bitte überprüfen Sie dies und versuchen es erneut!';
      this.modal = true;
    }
  }
}
