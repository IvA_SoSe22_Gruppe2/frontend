import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { LoginSelectors } from '../../store/selectors';
import { State } from '../../store/reducers';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  constructor(private readonly store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    this.store.select(LoginSelectors.adminLogin).subscribe((accepted) => {
      if (!accepted) {
        this.router.navigate(['/']);
      }
    });
  }
}
