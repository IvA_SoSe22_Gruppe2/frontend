import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CryptoService } from '../../service/crypto.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerGroup!: FormGroup;
  gender: string = 'Männlich';
  image: string | ArrayBuffer | null = '';

  modal: boolean = false;
  modalTitle: string = '';
  modalText: string = '';

  constructor(private http: HttpClient, private crypto: CryptoService) {}

  ngOnInit(): void {
    this.registerGroup = new FormGroup({
      firstname: new FormControl('', { validators: [Validators.required] }),
      name: new FormControl('', { validators: [Validators.required] }),
      height: new FormControl('', { validators: [Validators.required] }),
      weight: new FormControl('', { validators: [Validators.required] }),
      date: new FormControl('', { validators: [Validators.required] }),
      mail: new FormControl('', { validators: [Validators.required] }),
      password: new FormControl('', {
        validators: [Validators.required, Validators.minLength(6)],
      }),
    });

    this.http
      .get('/assets/profile-picture.jpg', { responseType: 'blob' })
      .subscribe((res) => {
        const reader = new FileReader();
        reader.readAsDataURL(res);
        reader.onloadend = () => {
          this.image = reader.result;
        };
      });
  }

  onUploadChange(evt: any) {
    const file = evt.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(e: any) {
    this.image = btoa(e.target.result);
  }

  markAsDirty(path: string) {
    this.registerGroup?.get(path)?.markAsDirty();
  }

  closeModal(value: boolean) {
    this.modal = false;
  }

  sendData() {
    if (this.registerGroup.valid && this.image) {
      let name = this.registerGroup.get('firstname')?.value;
      this.http
        .post<any>(
          environment.apiURL + '/User',
          {
            lastname: this.registerGroup.get('name')?.value,
            firstname: this.registerGroup.get('firstname')?.value,
            gender: this.gender,
            height: this.registerGroup.get('height')?.value,
            weight: this.registerGroup.get('weight')?.value,
            age: this.registerGroup.get('date')?.value + 'T00:00:00',
            profilePicture: this.image.toString().includes('data:image')
              ? this.image
              : 'data:image/jpeg;base64,' + this.image,
            email: this.registerGroup.get('mail')?.value,
            password: this.crypto.encrypt(
              this.registerGroup.get('password')?.value
            ),
            role: 'user',
            hashedEmail: this.crypto.encrypt(
              this.registerGroup.get('mail')?.value
            ),
          },
          { observe: 'response' }
        )
        .subscribe((msg) => {
          if (msg.status === 200) {
            this.modalTitle = 'Danke ' + name + '!';
            this.modalText =
              'Bitte überprüfe deine Emails und<br>aktiviere dein Konto!';
            this.modal = true;
          } else {
            this.modalTitle = 'Fehler';
            this.modalText =
              'Ein unbekannter Fehler ist aufgetreten.<br> Bitte versuchen Sie es später erneut!';
            this.modal = true;
          }
        });
    } else {
      this.modalTitle = 'Fehler';
      this.modalText =
        'Nicht alle Felder wurden richtig ausgefüllt. <br> Bitte überprüfen Sie dies und versuchen es erneut!';
      this.modal = true;
    }
    this.registerGroup.reset();
    this.image = '';
  }
}
