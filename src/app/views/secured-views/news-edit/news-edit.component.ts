import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { State } from '../../../store/reducers';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { LoginSelectors } from '../../../store/selectors';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-news-edit',
  templateUrl: './news-edit.component.html',
  styleUrls: ['./news-edit.component.scss'],
})
export class NewsEditComponent implements OnInit {
  adminLogged: boolean | undefined;
  supervisorLogged: boolean | undefined;

  newsGroup!: FormGroup;
  image: string = '';
  modal: boolean = false;
  modalTitle: string = '';
  modalText: string = '';

  constructor(
    private readonly store: Store<State>,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.store.select(LoginSelectors.adminLogin).subscribe((accepted) => {
      if (accepted) {
        this.adminLogged = accepted;
      }
    });
    this.store.select(LoginSelectors.supervisorLogin).subscribe((accepted) => {
      if (accepted) {
        this.supervisorLogged = accepted;
      }
    });

    if (this.adminLogged || this.supervisorLogged) {
    } else {
      this.router.navigate(['']);
    }

    this.newsGroup = new FormGroup({
      newsHeader: new FormControl('', { validators: [Validators.required] }),
      newsContent: new FormControl('', { validators: [Validators.required] }),
    });
  }

  onUploadChange(evt: any) {
    const file = evt.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(e: any) {
    this.image = btoa(e.target.result);
  }

  closeModal(value: boolean) {
    this.modal = false;
  }

  markAsDirty(path: string) {
    this.newsGroup?.get(path)?.markAsDirty();
  }

  sendData() {
    let currentTime = formatDate(new Date(), 'yyyy-MM-dd', 'en-US').toString();
    if (this.newsGroup.valid) {
      this.http
        .post<any>(
          environment.apiURL + '/News',
          {
            newsHeader: this.newsGroup.get('newsHeader')?.value,
            newsContent: this.newsGroup.get('newsContent')?.value,
            newsPicture: 'data:image/jpeg;base64,' + this.image,
            newsDate: currentTime + 'T00:00:00',
          },
          { observe: 'response' }
        )
        .subscribe((msg) => {
          if (msg.status === 200) {
            this.modalTitle = 'Die News wurden erfogreich veröffentlicht!';
            this.modal = true;
          } else {
            this.modalTitle = 'Fehler';
            this.modalText =
              'Ein unbekannter Fehler ist aufgetreten.<br> Bitte versuchen Sie es später erneut!';
            this.modal = true;
          }
        });
    } else {
      this.modalTitle = 'Fehler';
      this.modalText =
        'Nicht alle Felder wurden richtig ausgefüllt. <br> Bitte überprüfen Sie dies und versuchen es erneut!';
      this.modal = true;
    }
    this.newsGroup.reset();
    this.image = '';
  }
}
