import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../../store/reducers';
import { Router } from '@angular/router';
import { LoginSelectors } from '../../../store/selectors';

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.scss'],
})
export class PiechartComponent implements OnInit {
  adminLogged: boolean | undefined;
  supervisorLogged: boolean | undefined;

  constructor(private readonly store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    this.store.select(LoginSelectors.adminLogin).subscribe((accepted) => {
      if (accepted) {
        this.adminLogged = accepted;
      }
    });
    this.store.select(LoginSelectors.supervisorLogin).subscribe((accepted) => {
      if (accepted) {
        this.supervisorLogged = accepted;
      }
    });

    if (this.adminLogged || this.supervisorLogged) {
    } else {
      this.router.navigate(['']);
    }
  }
}
