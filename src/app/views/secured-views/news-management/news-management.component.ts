import { Component, OnInit } from '@angular/core';
import { LoginSelectors } from '../../../store/selectors';
import { Store } from '@ngrx/store';
import { State } from '../../../store/reducers';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-news-management',
  templateUrl: './news-management.component.html',
  styleUrls: ['./news-management.component.scss'],
})
export class NewsManagementComponent implements OnInit {
  modal: boolean = false;
  modalTitle: string = '';
  modalText: string = '';

  reload: boolean = false;

  adminLogged: boolean | undefined;
  supervisorLogged: boolean | undefined;

  currentNews: any;
  openEdit: boolean = false;

  newsEditGroup!: FormGroup;

  constructor(
    private readonly store: Store<State>,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.store.select(LoginSelectors.adminLogin).subscribe((accepted) => {
      if (accepted) {
        this.adminLogged = accepted;
      }
    });
    this.store.select(LoginSelectors.supervisorLogin).subscribe((accepted) => {
      if (accepted) {
        this.supervisorLogged = accepted;
      }
    });
    if (this.adminLogged || this.supervisorLogged) {
    } else {
      this.router.navigate(['']);
    }

    this.newsEditGroup = new FormGroup({
      newsHeader: new FormControl(),
      newsContent: new FormControl(),
    });
  }

  closeModal(value: boolean) {
    this.modal = false;
    this.reload = !this.reload;
  }

  openEditNews(e: any) {
    this.currentNews = e;
    this.newsEditGroup
      .get('newsHeader')
      ?.patchValue(this.currentNews.newsHeader);
    this.newsEditGroup
      .get('newsContent')
      ?.patchValue(this.currentNews.newsContent);
    this.openEdit = true;
  }

  editNews() {
    if (this.currentNews) {
      this.http
        .patch<any>(
          environment.apiURL + '/News/' + this.currentNews.newsId,
          {
            newsContent: this.newsEditGroup.get('newsContent')?.value
              ? this.newsEditGroup.get('newsContent')?.value
              : this.currentNews.newsContent,
            newsDate: this.currentNews.newsDate,
            newsPicture: this.currentNews.newsPicture,
            newsHeader: this.newsEditGroup.get('newsHeader')?.value
              ? this.newsEditGroup.get('newsHeader')?.value
              : this.currentNews.newsHeader,
          },
          { observe: 'response' }
        )
        .subscribe(
          (msg) => {
            if (msg.status === 200) {
              this.openEdit = false;
              this.modalTitle = 'Erfolg!';
              this.modalText = 'News wurde erfolgreich bearbeitet';
              this.modal = true;
            }
          },
          (error) => {
            console.log(error);
            this.modalTitle = 'Fehler!';
            this.modalText = 'Details finden Sie in der Konsole';
            this.modal = true;
          }
        );
    }
  }

  deleteNews(e: any) {
    this.http
      .delete<any>(environment.apiURL + '/News/' + e.newsId, {
        observe: 'response',
      })
      .subscribe(
        (msg) => {
          if (msg.status === 200) {
            this.modalTitle = 'Erfolg!';
            this.modalText = 'News wurde erfolgreich gelöscht';
            this.modal = true;
          }
        },
        (error) => {
          console.log(error);
          this.modalTitle = 'Fehler!';
          this.modalText = 'Details finden Sie in der Konsole';
          this.modal = true;
        }
      );
  }
}
