import { Component, OnInit } from '@angular/core';
import { LoginSelectors } from '../../../store/selectors';
import { State } from '../../../store/reducers';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.scss'],
})
export class BarchartComponent implements OnInit {
  adminLogged: boolean | undefined;
  supervisorLogged: boolean | undefined;

  constructor(private readonly store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    this.store.select(LoginSelectors.adminLogin).subscribe((accepted) => {
      if (accepted) {
        this.adminLogged = accepted;
      }
    });
    this.store.select(LoginSelectors.supervisorLogin).subscribe((accepted) => {
      if (accepted) {
        this.supervisorLogged = accepted;
      }
    });

    if (this.adminLogged || this.supervisorLogged) {
    } else {
      this.router.navigate(['']);
    }
  }
}
