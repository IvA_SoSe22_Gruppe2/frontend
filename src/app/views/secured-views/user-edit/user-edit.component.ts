import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../../store/reducers';
import { Router } from '@angular/router';
import { LoginSelectors } from '../../../store/selectors';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CryptoService } from '../../../service/crypto.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
})
export class UserEditComponent implements OnInit {
  modal: boolean = false;
  modalTitle: string = '';
  modalText: string = '';

  adminLogged: boolean | undefined;
  supervisorLogged: boolean | undefined;

  user$: Observable<HttpResponse<any>> | undefined;
  userEdit: boolean = false;
  acitveUser$: Observable<HttpResponse<any>> | undefined;
  activeUser: any;
  activeUserAge: number | undefined;

  gender: string = '';
  newGender: string = 'Männlich';

  image: string | ArrayBuffer | null = '';

  userAdd: boolean = false;

  patchGroup!: FormGroup;
  registerGroup!: FormGroup;

  constructor(
    private readonly store: Store<State>,
    private router: Router,
    private http: HttpClient,
    private crypto: CryptoService
  ) {}

  ngOnInit(): void {
    this.store.select(LoginSelectors.adminLogin).subscribe((accepted) => {
      if (accepted) {
        this.adminLogged = accepted;
      }
    });
    this.store.select(LoginSelectors.supervisorLogin).subscribe((accepted) => {
      if (accepted) {
        this.supervisorLogged = accepted;
      }
    });

    if (this.adminLogged || this.supervisorLogged) {
    } else {
      this.router.navigate(['']);
    }

    this.user$ = this.http.get<any>(environment.apiURL + '/User/getAllUsers', {
      observe: 'response',
    });

    this.patchGroup = new FormGroup({
      firstname: new FormControl(),
      lastname: new FormControl(),
      height: new FormControl(),
      weight: new FormControl(),
      age: new FormControl(),
      email: new FormControl(),
      password: new FormControl(),
    });

    this.registerGroup = new FormGroup({
      firstname: new FormControl('', { validators: [Validators.required] }),
      lastname: new FormControl('', { validators: [Validators.required] }),
      height: new FormControl('', { validators: [Validators.required] }),
      weight: new FormControl('', { validators: [Validators.required] }),
      age: new FormControl('', { validators: [Validators.required] }),
      email: new FormControl('', { validators: [Validators.required] }),
      password: new FormControl('', {
        validators: [Validators.required, Validators.minLength(6)],
      }),
    });

    this.http
      .get('/assets/profile-picture.jpg', { responseType: 'blob' })
      .subscribe((res) => {
        const reader = new FileReader();
        reader.readAsDataURL(res);
        reader.onloadend = () => {
          this.image = reader.result;
        };
      });
  }

  editUser(user: any) {
    this.acitveUser$ = this.http.get<any>(
      environment.apiURL + '/User/' + user.userId,
      {
        observe: 'response',
      }
    );
    this.acitveUser$.subscribe((res) => {
      this.activeUser = res.body[0];
      this.activeUserAge = this.activeUser.age.toString().substring(0, 10);
    });
    this.userEdit = true;
  }

  updateUser() {
    if ((this.patchGroup.touched || this.gender !== '') && this.activeUser) {
      this.http
        .patch<any>(
          environment.apiURL + '/User/Frontend/' + this.activeUser.userId,
          {
            lastName: this.patchGroup.get('lastname')?.value
              ? this.patchGroup.get('lastname')?.value
              : this.activeUser.lastName,
            firstName: this.patchGroup.get('firstname')?.value
              ? this.patchGroup.get('firstname')?.value
              : this.activeUser.firstName,
            gender: this.gender !== '' ? this.gender : this.activeUser.gender,
            height: this.patchGroup.get('height')?.value
              ? this.patchGroup.get('height')?.value
              : this.activeUser.height,
            weight: this.patchGroup.get('weight')?.value
              ? this.patchGroup.get('weight')?.value
              : this.activeUser.weight,
            age: this.patchGroup.get('age')?.value
              ? this.patchGroup.get('age')?.value + 'T00:00:00'
              : this.activeUser.age,
            profilePicture: this.activeUser.profilePicture,
            email: this.patchGroup.get('email')?.value
              ? this.patchGroup.get('email')?.value
              : this.activeUser.email,
            password: this.patchGroup.get('password')?.value
              ? this.crypto.encrypt(this.patchGroup.get('password')?.value)
              : this.activeUser.password,
            role: 'user',
            hashedEmail: this.patchGroup.get('email')?.value
              ? this.crypto.encrypt(this.patchGroup.get('email')?.value)
              : this.activeUser.hashedEmail,
          },
          { observe: 'response' }
        )
        .subscribe(
          (msg) => {
            if (msg.status === 200) {
              this.userEdit = false;
              this.modalTitle = 'Erfolg!';
              this.modalText = 'Nutzer wurde erfolgreich bearbeitet';
              this.modal = true;
            }
          },
          (error) => {
            console.log(error);
            this.modalTitle = 'Fehler!';
            this.modalText = 'Details finden Sie in der Konsole';
            this.modal = true;
          }
        );
    }
  }

  handleReaderLoaded(e: any) {
    this.image = btoa(e.target.result);
  }

  onUploadChange(evt: any) {
    const file = evt.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  addUser() {
    if (this.registerGroup.valid && this.image) {
      this.http
        .post<any>(
          environment.apiURL + '/User',
          {
            lastname: this.registerGroup.get('lastname')?.value,
            firstname: this.registerGroup.get('firstname')?.value,
            gender: this.newGender,
            height: this.registerGroup.get('height')?.value,
            weight: this.registerGroup.get('weight')?.value,
            age: this.registerGroup.get('age')?.value + 'T00:00:00',
            profilePicture: this.image.toString().includes('data:image')
              ? this.image
              : 'data:image/jpeg;base64,' + this.image,
            email: this.registerGroup.get('email')?.value,
            password: this.crypto.encrypt(
              this.registerGroup.get('password')?.value
            ),
            role: 'user',
            hashedEmail: this.crypto.encrypt(
              this.registerGroup.get('email')?.value
            ),
          },
          { observe: 'response' }
        )
        .subscribe(
          (msg) => {
            if (msg.status === 200) {
              this.userAdd = false;
              this.modalTitle = 'Erfolg!';
              this.modalText = 'Nutzer wurde erfolgreich angelegt';
              this.modal = true;
            }
          },
          (error) => {
            console.log(error);
            this.modalTitle = 'Fehler!';
            this.modalText = 'Details finden Sie in der Konsole';
            this.modal = true;
          }
        );
    } else {
      this.modalTitle = 'Fehler!';
      this.modalText = 'Es wurden nicht alle Felder ausgefüllt';
      this.modal = true;
    }
  }

  closeModal(value: boolean) {
    this.modal = false;
    this.user$ = this.http.get<any>(environment.apiURL + '/User/getAllUsers', {
      observe: 'response',
    });
  }

  deleteUser() {
    if (this.activeUser) {
      this.http
        .delete<any>(environment.apiURL + '/User/' + this.activeUser.userId, {
          observe: 'response',
        })
        .subscribe(
          (msg) => {
            if (msg.status === 200) {
              this.userEdit = false;
              this.modalTitle = 'Erfolg!';
              this.modalText = 'Nutzer wurde erfolgreich gelöscht';
              this.modal = true;
            }
          },
          (error) => {
            console.log(error);
            this.modalTitle = 'Fehler!';
            this.modalText = 'Details finden Sie in der Konsole';
            this.modal = true;
          }
        );
    }
  }
}
