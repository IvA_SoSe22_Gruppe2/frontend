import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../../store/reducers';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoginSelectors } from '../../../store/selectors';
import { ngxCsv } from 'ngx-csv';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import {
  ActivityEvent,
  ActivityEventDownload,
} from '../../../interfaces/event';
import { Activity } from '../../../interfaces/activity';
import { ActivityCollection } from '../../../interfaces/activityCollection';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss'],
})
export class DownloadComponent implements OnInit {
  adminLogged: boolean | undefined;
  supervisorLogged: boolean | undefined;

  modal: boolean = false;
  modalTitle: string = '';
  modalText: string = '';

  events$: Observable<any> | undefined;
  activities$: Observable<any> | undefined;

  results: Array<ActivityEventDownload> = [];

  constructor(
    private readonly store: Store<State>,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.store.select(LoginSelectors.adminLogin).subscribe((accepted) => {
      if (accepted) {
        this.adminLogged = accepted;
      }
    });
    this.store.select(LoginSelectors.supervisorLogin).subscribe((accepted) => {
      if (accepted) {
        this.supervisorLogged = accepted;
      }
    });

    if (this.adminLogged || this.supervisorLogged) {
    } else {
      this.router.navigate(['']);
    }

    this.events$ = this.http.get<any>(
      environment.apiURL + '/Event/getAllEvents',
      {
        observe: 'response',
      }
    );

    this.activities$ = this.http.get<any>(
      environment.apiURL + '/Activity/getAllActivities',
      {
        observe: 'response',
      }
    );

    this.calculateEventData();
  }

  calculateEventData() {
    this.activities$?.subscribe((act) => {
      this.events$?.subscribe((evt) => {
        console.log(evt);
        evt.body.forEach((one: ActivityEvent) => {
          let name: string = act.body.find(
            (test: Activity) => one.activityId === test.activityId
          ).activityName;
          this.results.push({
            eventId: one.eventId,
            activityName: name,
            userId: one.userId,
            avgSpeed: one.avgSpeed,
            calories: one.calories,
            distanceInKm: one.distanceInKm,
            duration: Math.round(one.duration / (60 * 10000000)),
            startDate: one.startDate,
            endDate: one.endDate,
          });
        });
      });
    });
  }

  closeModal(value: boolean) {
    this.modal = false;
  }

  fileDownload() {
    var options = {
      fieldSeparator: ';',
      quoteStrings: '"',
      decimalseparator: ',',
      showLabels: true,
      showTitle: true,
      title: 'Aktivitäten der Nutzer',
      useBom: true,
      noDownload: false,
      headers: [
        'ID',
        'Aktivität',
        'User-ID',
        'Durchschnittliche Geschwindigkeit in Km/h',
        'Kalorien',
        'Distanz in KM',
        'Dauer in Minuten',
        'Start-Zeitpunkt',
        'End-Zeitpunkt',
      ],
    };

    new ngxCsv(this.results, 'Aktivitätsdaten_Nutzer', options);
    this.modalTitle = 'Die Aktivitäten wurden erfolgreich heruntergeladen!';
    this.modal = true;
  }
}
