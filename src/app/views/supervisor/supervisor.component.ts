import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import { Router } from '@angular/router';
import { LoginSelectors } from '../../store/selectors';

@Component({
  selector: 'app-supervisor',
  templateUrl: './supervisor.component.html',
  styleUrls: ['./supervisor.component.scss'],
})
export class SupervisorComponent implements OnInit {
  constructor(private readonly store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    this.store.select(LoginSelectors.supervisorLogin).subscribe((accepted) => {
      if (!accepted) {
        this.router.navigate(['/']);
      }
    });
  }
}
