import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SwiperModule } from 'swiper/angular';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers } from './store/reducers';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { NewsSwiperComponent } from './components/newsSwiper/newsSwiper.component';
import { LoginComponent } from './views/login/login.component';
import { HomeComponent } from './views/home/home.component';
import { RegisterComponent } from './views/register/register.component';
import { ContactComponent } from './views/contact/contact.component';
import { AboutComponent } from './views/about/about.component';
import { DataProtectionComponent } from './views/data-protection/data-protection.component';
import { ImprintComponent } from './views/imprint/imprint.component';
import { OpenerComponent } from './components/opener/opener.component';
import { ModalComponent } from './components/modal/modal.component';
import { NewsComponent } from './views/news/news.component';
import { AdminTeaserComponent } from './components/adminTeaser/admin-teaser.component';
import { DividerComponent } from './components/divider/divider.component';
import { KeyPointsComponent } from './components/key-points/key-points.component';
import { ChartsComponent } from './components/charts/charts.component';

import { AdminComponent } from './views/admin/admin.component';
import { SupervisorComponent } from './views/supervisor/supervisor.component';
import { VerifyComponent } from './views/verify/verify.component';
import { PiechartComponent } from './views/secured-views/piechart/piechart.component';
import { BarchartComponent } from './views/secured-views/barchart/barchart.component';
import { TableComponent } from './views/secured-views/table/table.component';
import { DownloadComponent } from './views/secured-views/download/download.component';
import { UserEditComponent } from './views/secured-views/user-edit/user-edit.component';
import { AdminEditComponent } from './views/secured-views/admin-edit/admin-edit.component';
import { NewsEditComponent } from './views/secured-views/news-edit/news-edit.component';
import { SecondLevelHeaderComponent } from './components/second-level-header/second-level-header.component';
import { SafePipe } from './pipes/safePipe';
import { NewsManagementComponent } from './views/secured-views/news-management/news-management.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NewsSwiperComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    ContactComponent,
    AboutComponent,
    DataProtectionComponent,
    ImprintComponent,
    OpenerComponent,
    ModalComponent,
    NewsComponent,
    AdminTeaserComponent,
    DividerComponent,
    KeyPointsComponent,
    ChartsComponent,

    AdminComponent,
    SupervisorComponent,
    VerifyComponent,
    PiechartComponent,
    BarchartComponent,
    TableComponent,
    DownloadComponent,
    UserEditComponent,
    AdminEditComponent,
    NewsEditComponent,
    SecondLevelHeaderComponent,
    SafePipe,
    NewsManagementComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    SwiperModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatIconModule,
    NgxChartsModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
