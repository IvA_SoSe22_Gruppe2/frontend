import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { CryptoSelectors } from '../store/selectors';
import { Store } from '@ngrx/store';
import { State } from '../store/reducers';

@Injectable({
  providedIn: 'root',
})
export class CryptoService {
  key: string | undefined;
  iv: string | undefined;

  constructor(private readonly store: Store<State>) {
    this.store.select(CryptoSelectors.getKey).subscribe((key) => {
      if (key) {
        this.key = key;
      }
    });
    this.store.select(CryptoSelectors.getIV).subscribe((iv) => {
      if (iv) {
        this.iv = iv;
      }
    });
  }

  encrypt(plain: string): string {
    if (this.key && this.iv) {
      return CryptoJS.AES.encrypt(plain, CryptoJS.enc.Utf8.parse(this.key), {
        iv: CryptoJS.enc.Utf8.parse(this.iv),
        format: CryptoJS.format.Hex,
      }).toString();
    } else return 'error';
  }

  decrypt(cypher: string): string {
    if (this.key && this.iv) {
      return CryptoJS.AES.decrypt(cypher, CryptoJS.enc.Utf8.parse(this.key), {
        iv: CryptoJS.enc.Utf8.parse(this.iv),
        format: CryptoJS.format.Hex,
      }).toString(CryptoJS.enc.Utf8);
    } else return 'error';
  }
}
